---
title: "csv2ical"
date: 2019-12-30T13:16:25+01:00
archives: "2019"
tags: [tcl, csv, ical]
author: "André Bögge"
summary: "Hier ein paar Zeilen Tcl-Code die zeigen, wie man aus einer einfachen csv-Datei eine ics-Datei im ical-Format macht. "
---

Hier ein paar Zeilen Tcl-Code die zeigen, wie man aus einer einfachen csv-Datei eine ics-Datei im ical-Format macht. 
Diese kann dann ohne weiteres auf einer Website veröffentlicht oder aber in Outlook oder Gmail in den Kalender importiert werden.

Hier erst einmal der Aufbau der csv-Datei:
``` 
Datum;Uhrzeit (von);Uhrzeit (bis);Thema;Beschreibung;Ort;Kategorie
03.01.2020;19:00;22:00;Essen mit Nicole;;Soest - Zwiebel;Privat
08.01.2020;17:00;18:30;Training F-Jugend;;Sportplatz;Fußball
26.01.2020;;;Geburtstag Bert;;;Geburtstag
```

Hier nun ein paar Zeilen Code, um aus der CSV-Datei eine Datei im ical-Format zu generieren. Der Name des Kalenders wird dabei
aus dem Dateinamen der CSV-Datei generiert. Bei der ersten Implementierung wird die Überschriftenzeile im folgenden 
Quellcode einfach beim Einlesen ignoriert.   
Wird für den Termin keine Uhrzeit definiert, so wird aus dem Termin ein ganztägiger
Termin generiert. Hat ein Termin eine Start- aber keine Endzeit, dann wird der Termin standardmäßig mit einer Dauer von einer Stunde
angelegt.

Code:
```tcl
package require csv

## EXAMPLE TARGET FORMAT ##
# BEGIN:VCALENDAR
# VERSION:2.0
# PRODID:http://www.aboegge.de
# CALSCALE:GREGORIAN
# METHOD:PUBLISH
# X-WR-CALNAME:Mein Kalender 2020
# X-WR-CALDESC:Mein privater Kalender 2020
# X-WR-TIMEZONE:Europe/Berlin
# BEGIN:VTIMEZONE
# TZID:Europe/Berlin
# X-LIC-LOCATION:Europe/Berlin
# END:VTIMEZONE
# BEGIN:VEVENT
# DTSTART:20200103T193000Z
# DTEND:20200103T220000Z
# DTSTAMP:20191217T180000Z
# UID:00001g@aboegge.de
# CLASS:PUBLIC
# SUMMARY:Date mit Nicole
# DESCRIPTION:
# LOCATION:Soest
# END:VEVENT
# END:VCALENDAR

proc csv2ical {fName} {
  set fh [open $fName]
  fconfigure $fh -enc utf-8
  
  set out [open [file root $fName].ics w]
  fconfigure $out -enc utf-8
  
  set genTS [clock format [clock seconds] -format "%Y%m%dT%H%M00Z"]
  
  puts $out "BEGIN:VCALENDAR"
  puts $out "VERSION:2.0"
  puts $out "PRODID:http://www.aboegge.de"
  puts $out "CALSCALE:GREGORIAN"
  puts $out "METHOD:PUBLISH"
  puts $out "X-WR-CALNAME:[file root [file tail $fName]]"
  # X-WR-CALDESC:Mein Kalender 2020
  puts $out "X-WR-TIMEZONE:Europe/Berlin"
  puts $out "BEGIN:VTIMEZONE"
  puts $out "TZID:Europe/Berlin"
  puts $out "X-LIC-LOCATION:Europe/Berlin"
  puts $out "END:VTIMEZONE"
  
  # HEAD LINE!
  gets $fh line
  
  set i 0
  while {![eof $fh]} {
    incr i
    gets $fh line
    if {[string trim $line] eq ""} {continue} 
    set line [csv::split $line ";" \"]
    foreach {date begin end summary description location} $line {}
    puts $out "BEGIN:VEVENT"        
    
    if {[string trim $begin] eq ""} {
      # If no begin is set, set it to all-day
      set begin [clock format [clock scan "$date" -format "%d.%m.%Y"] -format "%Y%m%d"]
      set end [clock format [clock add [clock scan "$date" -format "%d.%m.%Y"] 1 day] -format "%Y%m%d"]
      puts $out "DTSTART;VALUE=DATE:$begin"
      puts $out "DTEND;VALUE=DATE:$end"
    } else { 
      # If end is not set add 1 hour to begin as standard 
      if {[string trim $end] eq ""} { 
        set end   [clock format [clock add [clock scan "$date $begin" -format "%d.%m.%Y %H:%M"] 1 hour] -format "%Y%m%dT%H%M00Z"]
      } else {
        set end   [clock format [clock scan "$date $end" -format "%d.%m.%Y %H:%M"] -format "%Y%m%dT%H%M00Z"]
      }
      set begin [clock format [clock scan "$date $begin" -format "%d.%m.%Y %H:%M"] -format "%Y%m%dT%H%M00Z"]
      puts $out "DTSTART:$begin"
      puts $out "DTEND:$end"      
    }
    
    puts $out "DTSTAMP:$genTS"
    puts $out "UID:[clock clicks]_${i}@aboegge.de"
    puts $out "CLASS:PUBLIC"
    puts $out "SUMMARY:$summary"
    puts $out "DESCRIPTION:$description"
    puts $out "LOCATION:$location"
    puts $out "END:VEVENT"  
  }
  
  puts $out "END:VCALENDAR"
  close $out
  close $fh
}
```   